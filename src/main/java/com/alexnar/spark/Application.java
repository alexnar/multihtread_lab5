package com.alexnar.spark;

import static com.alexnar.spark.LogRecordConverter.OUTPUT_DATE_FORMAT;
import static org.apache.spark.sql.functions.window;

import java.util.Objects;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.*;

public class Application {
  public static void main(String[] args) {
    SparkSession session = SparkSession.builder().master("local").appName("lab5").getOrCreate();
    JavaSparkContext jsc = new JavaSparkContext(session.sparkContext());
    JavaRDD<LogRecord> input = jsc.textFile("hdfs://master:9000/logs/access_log_Jul95")
            .map(LogRecordConverter::convertStringToLogEntry)
            .filter(Objects::nonNull);
    Dataset<Row> dataSet = session.createDataFrame(input, LogRecord.class);

    Tasks.task1(dataSet);
    Tasks.task2(dataSet);
    Tasks.task3(dataSet);
  }
}