package com.alexnar.spark;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogRecordConverter {
  public static final String PATTERN =
          "^(\\S+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+\\-]\\d{4})] \"(\\S+) (\\S+) (\\S+)\" (\\d{3}) (\\d+)";
  public static final String INPUT_DATE_FORMAT = "dd/MMM/yyyy:HH:mm:ss Z";
  public static final String OUTPUT_DATE_FORMAT = "dd/MMM/yyyy";

  public static LogRecord convertStringToLogEntry(String str) {
    Pattern logPattern = Pattern.compile(PATTERN);
    Matcher matcher = logPattern.matcher(str);
    if (!matcher.find()) return null;
    String request = matcher.group(6);
    String group = matcher.group(5);
    String returnCode = matcher.group(8);
    String dateStr = formatDate(matcher.group(4));
    return new LogRecord(request, group, returnCode, dateStr);
  }

  private static String formatDate(String datetime) {
    DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern(INPUT_DATE_FORMAT, Locale.US);
    DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern(OUTPUT_DATE_FORMAT, Locale.US);
    return LocalDate.parse(datetime, inputFormat).format(outputFormat);
  }
}
