package com.alexnar.spark;

import java.io.Serializable;

public class LogRecord implements Serializable {
  private String request;
  private String method;
  private String returnCode;
  private String dateStr;

  public LogRecord(String request, String method, String returnCode, String dateStr) {
    this.request = request;
    this.method = method;
    this.returnCode = returnCode;
    this.dateStr = dateStr;
  }

  public String getRequest() {
    return request;
  }

  public void setRequest(String request) {
    this.request = request;
  }

  public String getMethod() {
    return method;
  }

  public void setMethod(String method) {
    this.method = method;
  }

  public String getReturnCode() {
    return returnCode;
  }

  public void setReturnCode(String returnCode) {
    this.returnCode = returnCode;
  }

  public String getDateStr() {
    return dateStr;
  }

  public void setDateStr(String dateStr) {
    this.dateStr = dateStr;
  }
}