package com.alexnar.spark;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import static com.alexnar.spark.LogRecordConverter.OUTPUT_DATE_FORMAT;
import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.functions.date_format;

public class Tasks {
  public static void task1(Dataset<Row> dataSet) {
    dataSet.filter(col("returnCode").between(500, 599))
            .groupBy("request")
            .count()
            .select("request", "count")
            .sort(desc("count"))
            .coalesce(1)
            .toJavaRDD()
            .saveAsTextFile("hdfs://master:9000/res1");
  }

  public static void task2(Dataset<Row> dataSet) {
    dataSet.groupBy("method", "returnCode", "dateStr")
            .count()
            .filter(col("count").geq(10))
            .select("dateStr", "method", "returnCode", "count")
            .sort("dateStr")
            .coalesce(1)
            .toJavaRDD()
            .saveAsTextFile("hdfs://master:9000/res2");
  }

  public static void task3(Dataset<Row> dataSet) {
    dataSet.filter(col("returnCode").between(400, 599))
            .groupBy(window(to_date(col("dateStr"), OUTPUT_DATE_FORMAT), "1 week", "1 day"))
            .count()
            .select(date_format(col("window.start"), OUTPUT_DATE_FORMAT),
                    date_format(col("window.end"), OUTPUT_DATE_FORMAT),
                    col("count"))
            .sort("window.start")
            .coalesce(1)
            .toJavaRDD()
            .saveAsTextFile("hdfs://master:9000/res3");
  }
}
