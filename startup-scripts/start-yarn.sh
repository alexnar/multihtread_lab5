#!/bin/bash

sh /startup-scripts/start.sh
spark-submit --master yarn \
             --deploy-mode client \
             --class com.alexnar.spark.Application /home/lab/jar/lab5-1.0.jar