FROM openjdk:8

ENV HADOOP_HOME /hadoop-3.1.2
ENV HADOOP_CONF $HADOOP_HOME/etc/hadoop
ENV SPARK_HOME /spark-2.4.3-bin-hadoop2.7
ENV PATH=$PATH:$HADOOP_HOME/bin/:$HADOOP_HOME/sbin/:$SPARK_HOME/bin/

RUN apt-get update
RUN apt-get install -y nano
RUN apt-get install -y wget
RUN apt-get -y install ssh
RUN apt-get -y install openssh-server

RUN ssh-keygen -t rsa -f ~/.ssh/id_rsa -P '' && \
    cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
COPY ssh/* $HOME/.ssh/config

RUN mkdir /home/hadoop-install
RUN mkdir /home/spark-install
RUN wget http://apache-mirror.rbc.ru/pub/apache/hadoop/common/hadoop-3.1.2/hadoop-3.1.2.tar.gz -P /home/hadoop-install
RUN wget http://apache-mirror.rbc.ru/pub/apache/spark/spark-2.4.3/spark-2.4.3-bin-hadoop2.7.tgz -P /home/spark-install
RUN tar xvzf /home/hadoop-install/hadoop-3.1.2.tar.gz
RUN tar xvzf /home/spark-install/spark-2.4.3-bin-hadoop2.7.tgz

COPY hadoop-conf/* $HADOOP_CONF/
RUN mkdir -p /usr/local/hadoop/hdfs/datanode
RUN mkdir -p /usr/local/hadoop/hdfs/namenode
RUN hdfs namenode -format

ENV HADOOP_CONF_DIR $HADOOP_CONF

COPY build/libs/lab5-1.0.jar /home/lab/jar/
COPY data/access_log_Jul95 /home/logs/access_log_Jul95
COPY startup-scripts/* /startup-scripts/
RUN chmod +x /startup-scripts/start.sh
RUN chmod +x /startup-scripts/start-standalone.sh
RUN chmod +x /startup-scripts/start-yarn.sh

CMD [ "/bin/bash", "-c", "service ssh start; tail -f /dev/null"]




